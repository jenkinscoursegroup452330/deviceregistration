package com.infy.devicedemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.infy.devicedemo.dto.Devicedetailsdto;
import com.infy.devicedemo.service.Deviceservice;

@RestController
@RequestMapping("/device")
public class devicecontroller {
	
	@Autowired
	private Deviceservice service;
	@PostMapping("/save")
	public Devicedetailsdto savedevice(@RequestBody Devicedetailsdto dto) {
		return service.saveorder(dto);
	}
	
	@GetMapping("/details")
	public List<Devicedetailsdto>getalldetails(){
		return service.getalldevicedetails();
	}
	@GetMapping("/details/{id}")
	public Devicedetailsdto getadetailsbyId(@PathVariable String id){
		return service.getdetalsbyId(id);
	}

}
