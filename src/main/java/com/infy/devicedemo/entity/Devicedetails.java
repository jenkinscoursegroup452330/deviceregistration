package com.infy.devicedemo.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;


//“deviceId “:”<string>”, 	(PK)
//“deviceName“:”<string>”, 
//“deviceDescription“:”<string>”, 
//“totalAvailableDevice”:”<integer?>”, 
//“amount”:”<dollar amt>”,
//“device_status”: “<boolean>” value : true or false



@Entity
public class Devicedetails {
@Id
private String deviceId;
private String deviceName;
private String description;
private int availabledevices;
private int amount;
private boolean deviceStatus;


public String getDeviceId() {
	return deviceId;
}
public void setDeviceId(String deviceId) {
	this.deviceId = deviceId;
}
public String getDeviceName() {
	return deviceName;
}
public void setDeviceName(String deviceName) {
	this.deviceName = deviceName;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public int getAvailabledevices() {
	return availabledevices;
}
public void setAvailabledevices(int availabledevices) {
	this.availabledevices = availabledevices;
}
public int getAmount() {
	return amount;
}
public void setAmount(int amount) {
	this.amount = amount;
}
public boolean isDeviceStatus() {
	return deviceStatus;
}
public void setDeviceStatus(boolean deviceStatus) {
	this.deviceStatus = deviceStatus;
}



}
