package com.infy.devicedemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevicedemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevicedemoApplication.class, args);
	}

}
