package com.infy.devicedemo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infy.devicedemo.dto.Devicedetailsdto;
import com.infy.devicedemo.entity.Devicedetails;
import com.infy.devicedemo.repository.DeviceRepository;
@Service
public class DeviceserviceImpl implements Deviceservice{
	@Autowired
	private DeviceRepository repo;
	@Autowired
	private Devicedetailsdto devicedto;
	@Override
	public Devicedetailsdto saveorder(Devicedetailsdto dto) {
		// TODO Auto-generated method stub
		Devicedetails device=devicedto.convertIntoEntity(dto);
		repo.save(device);
		Devicedetailsdto devicedto1= dto.convertIntoDto(device);
		return devicedto1;
		
	}
	@Override
	public List<Devicedetailsdto> getalldevicedetails() {
		// TODO Auto-generated method stub
		List<Devicedetails> list1=repo.findAll();
		return devicedto.createdtolist(list1);
	
	}
	@Override
	public Devicedetailsdto getdetalsbyId(String id) {
		// TODO Auto-generated method stub
		Optional<Devicedetails> opdevice=repo.findById(id);
		Devicedetails device=opdevice.get();
		return devicedto.convertIntoDto(device);
	}
	
	
		
	

}
